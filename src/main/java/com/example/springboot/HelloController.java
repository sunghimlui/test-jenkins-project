package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/health")
	public String index() {
		System.out.println("Looking healthy mate");
		return "Looking healthy";
	}

}
